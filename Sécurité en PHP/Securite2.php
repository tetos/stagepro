<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Connexion</title>
</head>
<body>
<!-- Créez une page de connexion sécurisée en PHP en utilisant des sessions pour authentifier les utilisateurs. -->
<?php
session_start();

// Vérifier si l'utilisateur est déjà connecté
if (isset($_SESSION['utilisateur'])) {
    header('Location: accueil.php');
    exit();
}

// Vérifier si le formulaire a été soumis
if (isset($_POST['nom_utilisateur']) && isset($_POST['mot_de_passe'])) {
    // Vérifier les informations d'identification
    if ($_POST['nom_utilisateur'] === 'admin' && $_POST['mot_de_passe'] === 'password') {
        // Authentification réussie, enregistrer l'utilisateur dans la session
        $_SESSION['utilisateur'] = $_POST['nom_utilisateur'];
        header('Location: accueil.php');
        exit();
    } else {
        // Informations d'identification incorrectes, afficher un message d'erreur
        $messageErreur = 'Nom d\'utilisateur ou mot de passe incorrect.';
    }
}
?>

<h1>Connexion</h1>
    <?php if (isset($messageErreur)) { ?>
        <p><?php echo $messageErreur; ?></p>
    <?php } ?>
    <form method="post">
        <div>
            <label for="nom_utilisateur">Nom d'utilisateur :</label>
            <input type="text" name="nom_utilisateur" required>
        </div>
        <div>
            <label for="mot_de_passe">Mot de passe :</label>
            <input type="password" name="mot_de_passe" required>
        </div>
        <div>
            <button type="submit">Se connecter</button>
        </div>
    </form>
    Explications :

<!-- - La première partie du code vérifie si l'utilisateur est déjà connecté en vérifiant s'il existe une variable de session "utilisateur". Si c'est le cas, la page est redirigée vers la page d'accueil.
- La deuxième partie du code vérifie si le formulaire de connexion a été soumis en vérifiant si les variables POST "nom_utilisateur" et "mot_de_passe" existent. Si c'est le cas, les informations d'identification sont vérifiées. Si elles sont correctes, l'utilisateur est enregistré dans la session et redirigé vers la page d'accueil. Sinon, un message d'erreur est affiché.
- La page HTML contient un formulaire de connexion avec deux champs : "nom_utilisateur" et "mot_de_passe". Le formulaire est soumis en méthode POST. Si une erreur est survenue lors de la tentative de connexion, un message d'erreur est affiché à l'utilisateur. -->
</body>
</html>