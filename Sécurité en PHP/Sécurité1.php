<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Securite en php</title>
</head>
<body>
    <!-- Écrivez une fonction echapperHTML qui prend une chaîne de caractères en entrée et échappe les caractères spéciaux pour prévenir les attaques XSS. -->
    <?php 
        // Voici une implémentation possible de la fonction echapperHTML en PHP
        function echapperHTML($chaine) {
            return htmlspecialchars($chaine, ENT_QUOTES | ENT_HTML5, 'UTF-8');
        }
        // <! --Cette fonction utilise la fonction PHP "htmlspecialchars" pour
        // échapper les caractères spéciaux dans la chaîne de caractères donnée
        // en entrée. Les options "ENT_QUOTES" et "ENT_HTML5" permettent de prendre
        // en compte les guillemets doubles et simples, ainsi que les caractères
        // spéciaux introduits par HTML5. Le dernier argument "UTF-8" indique que
        // la chaîne de caractères est encodée en UTF-8.

        $texte = '<script>alert("Hello world!");</script>';
        $texteEchappe = echapperHTML($texte);
        echo $texteEchappe; // Affiche '&lt;script&gt;alert(&quot;Hello world!&quot;);&lt;/script&gt;'
// Dans cet exemple, la chaîne de caractères $texte contient du code JavaScript qui affiche une alerte. En utilisant la fonction echapperHTML, nous obtenons une version échappée de cette chaîne qui peut être affichée en toute sécurité dans une page HTML.
    ?>
</body>
</html>