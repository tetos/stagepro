<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <!-- Créez une classe "Voiture" avec des propriétés telles que la marque, le modèle, et la vitesse actuelle. Ajoutez des méthodes pour accélérer et freiner la voiture.
    Ensuite, créez une instance de cette classe et simulez un trajet en modifiant la vitesse. -->

    <?php 
        class Voiture {
            private $marque;
            private $modele;
            private $vitesse;
          
            public function __construct($marque, $modele) {
              $this->marque = $marque;
              $this->modele = $modele;
              $this->vitesse = 0;
            }
          
            public function accelerer($vitesse) {
              $this->vitesse += $vitesse;
            }
          
            public function freiner($vitesse) {
              if ($this->vitesse - $vitesse < 0) {
                $this->vitesse = 0;
              } 
                $this->vitesse -= $vitesse;
              
            }
          
            public function getVitesse() {
              return $this->vitesse;
            }
          }
          
          // Création d'une instance de la classe "Voiture"
          $maVoiture = new Voiture("Renault", "Clio");
          
          // Simulation d'un trajet
          $maVoiture->accelerer(50);
          echo "Vitesse actuelle : " . $maVoiture->getVitesse() . " km/h<br>";
          $maVoiture->freiner(20);
          echo "Vitesse actuelle : " . $maVoiture->getVitesse() . " km/h<br>";
          $maVoiture->accelerer(30);
          echo "Vitesse actuelle : " . $maVoiture->getVitesse() . " km/h<br>";
          
    ?>
</body>
</html>