<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <!-- Créez une classe "Voiture" avec des propriétés telles que la marque, le modèle, et la vitesse actuelle. Ajoutez des méthodes pour accélérer et freiner la voiture. -->
    <?php 
        class Voiture {
            private $marque;
            private $modele;
            private $vitesse;
          
            public function __construct($marque, $modele) {
              $this->marque = $marque;
              $this->modele = $modele;
              $this->vitesse = 0;
            }
          
            public function accelerer($vitesse) {
              $this->vitesse += $vitesse;
            }
          
            public function freiner($vitesse) {
              if ($this->vitesse >= $vitesse) {
                $this->vitesse -= $vitesse;
              } 
                $this->vitesse = 0;
              
            }
          
            public function getMarque() {
              return $this->marque;
            }
          
            public function getModele() {
              return $this->modele;
            }
          
            public function getVitesse() {
              return $this->vitesse;
            }
          }
          
          // Exemple d'utilisation :
          $voiture1 = new Voiture("Renault", "Clio");
          $voiture1->accelerer(50);
          echo "La voiture " . $voiture1->getMarque() . " " . $voiture1->getModele() . " roule à " . $voiture1->getVitesse() . " km/h.<br>"; // affiche "La voiture Renault Clio roule à 50 km/h."
          $voiture1->freiner(20);
          echo "La voiture " . $voiture1->getMarque() . " " . $voiture1->getModele() . " roule à " . $voiture1->getVitesse() . " km/h.<br>"; // affiche "La voiture Renault Clio roule à 30 km/h."
          
    ?>
</body>
</html>