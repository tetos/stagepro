<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <!-- Créez une fonction rechercherMot qui prend un fichier et un mot en entrée, puis renvoie le nombre d'occurrences de ce mot dans le fichier. -->
    <?php 
        function rechercherMot($fichier, $mot) {
            $contenu = file_get_contents($fichier);
            $occurrences = substr_count($contenu, $mot);
            return $occurrences;
          }
          
          // Exemple d'utilisation
        //   $nbOccurrences = rechercherMot("monFichier.txt", "Bonjour");
        //   echo "Le mot 'Bonjour' apparaît " . $nbOccurrences . " fois dans le fichier.";
          
    ?>
    <!-- Dans cet exemple, la fonction "rechercherMot" prend en 
    entrée un nom de fichier et un mot à rechercher. Elle utilise 
    la fonction "file_get_contents" pour récupérer le contenu du 
    fichier, puis la fonction "substr_count" pour compter le nombre
     d'occurrences du mot dans le contenu. Enfin, elle renvoie ce nombre d'occurrences -->
</body>
</html>