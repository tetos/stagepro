<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Gestion de Fichiers Avancée</title>
</head>
<body>
<!-- Écrivez une fonction copierFichier qui prend deux noms de fichiers en entrée et copie le contenu du premier fichier dans le second. -->
    <?php 
        function copierFichier($source, $destination) {
            $contenu = file_get_contents($source); // lecture du contenu du premier fichier
            file_put_contents($destination, $contenu); // écriture du contenu dans le second fichier
          }
          //Exemple d'utilisation :
          //copierFichier("fichier1.txt", "fichier2.txt");

    ?>
    <!-- Cette fonction copie le contenu du fichier "fichier1.txt" 
    dans le fichier "fichier2.txt". Si le fichier de destination 
    existe déjà, son contenu sera remplacé par celui du fichier source. -->
</body>
</html>