<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Somme de deux nombres</title>
</head>
<body>
    <!-- Créez une fonction nommée sommeDeuxNombres qui prend deux nombres en entrée et renvoie leur somme -->
    <?php
        function sommeDeuxNombres($a, $b) {
            $calcul = $a + $b;
            return $calcul;
        } 
        $resultat_1 = sommeDeuxNombres(582, 6000);
        $resultat_2 = sommeDeuxNombres(100, 2450);
        
    ?>
    <p>premier calcul = <?php echo $resultat_1 ?></p>
    <p>second calcul = <?php echo $resultat_2 ?></p>
</body>
</html>