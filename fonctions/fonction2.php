<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>factorielle</title>
</head>
<body>
    <!-- Créez une fonction nommée factorielle qui calcule la factorielle d'un nombre. -->
    <?php 
        function factorielle ($nbr) {
            if($nbr === 0)
            return 1;
           else
            return $nbr*factorielle($nbr-1);
        }
    ?>
    <p>5! = <?php echo factorielle(5) ?></p>
    <p>125! = <?php echo factorielle(125) ?></p>
    <p>44! = <?php echo factorielle(44) ?></p>
    <p>80! = <?php echo factorielle(80) ?></p>
    <p>2000000! = <?php echo factorielle(2000000) ?></p>
    <p>650000! = <?php echo factorielle(650000) ?></p>
    <p>800! = <?php echo factorielle(800) ?></p>
    <p>2250! = <?php echo factorielle(2250) ?></p>
</body>
</html>