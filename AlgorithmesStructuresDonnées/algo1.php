<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Algorithmes et Structures de Données</title>
</head>

<body>
    <!-- Implémentez un algorithme de tri, comme le tri rapide (QuickSort), pour trier un tableau de nombres. -->
    <?php
    // Fonction de tri rapide (QuickSort)
    function quickSort($tableau)
    {
        // Cas de base : le tableau est vide ou ne contient qu'un élément
        if (count($tableau) <= 1) {
            return $tableau;
        }

        // Choix du pivot
        $pivot = $tableau[0];

        // Séparation des éléments inférieurs et supérieurs au pivot
        $inf = array();
        $sup = array();
        for ($i = 1; $i < count($tableau); $i++) {
            if ($tableau[$i] < $pivot) {
                $inf[] = $tableau[$i];
            } 
                $sup[] = $tableau[$i];
            
        }

        // Récursion sur les deux sous-tableaux inférieurs et supérieurs
        return array_merge(quickSort($inf), array($pivot), quickSort($sup));
    }

    // Exemple d'utilisation de la fonction de tri rapide (QuickSort)
    $tableau = array(5, 8, 1, 3, 6, 2, 7, 4);
    $tableauTrie = quickSort($tableau);
    echo "Tableau trié : ";
    foreach ($tableauTrie as $element) {
        echo "$element ";
    }
    ?>
    <!-- Dans cet exemple, nous créons une fonction "quickSort" qui prend en entrée un tableau de nombres à trier. Nous commençons par définir le cas de base où le tableau est vide ou ne contient qu'un élément, dans ce cas nous renvoyons simplement le tableau tel quel.

    Nous choisissons ensuite un pivot, dans notre exemple nous prenons le premier élément du tableau. Nous séparons ensuite les éléments inférieurs et supérieurs au pivot dans deux sous-tableaux distincts en parcourant le tableau avec une boucle "for".

    Nous appliquons ensuite la récursion sur les deux sous-tableaux inférieurs et supérieurs en appelant la fonction "quickSort" sur chacun d'entre eux. Nous fusionnons finalement les trois tableaux (inférieurs, pivot, supérieurs) avec la fonction "array_merge" pour obtenir le tableau trié final.

    Enfin, nous testons notre fonction de tri rapide en créant un tableau d'exemple et en l'affichant trié avec une boucle "foreach". -->
</body>

</html>