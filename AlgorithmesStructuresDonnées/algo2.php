<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Algorithmes et Structures de Données</title>
</head>

<body>
    <!-- Créez une structure de données de file (queue) en utilisant des tableaux PHP et ajoutez des opérations d'enfilement et de défilement. -->
    <?php
    class File
    {
        private $elements;

        public function __construct()
        {
            $this->elements = array();
        }

        public function enfile($element)
        {
            array_push($this->elements, $element);
        }

        public function defile()
        {
            if ($this->estVide()) {
                return null;
            } 
                return array_shift($this->elements);
            
        }

        public function estVide()
        {
            return empty($this->elements);
        }
    }

    // Exemple d'utilisation de la file
    $file = new File();
    $file->enfile(1);
    $file->enfile(2);
    $file->enfile(3);
    echo $file->defile(); // Affiche 1
    echo $file->defile(); // Affiche 2
    echo $file->defile(); // Affiche 3
    echo $file->defile(); // Affiche null (la file est vide)
    ?>

</body>

</html>