<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>ConceptsBasePOO</title>
</head>
<body>
<!-- Créez une classe PHP nommée "Personne" avec des propriétés telles que le nom et l'âge, ainsi que des méthodes pour afficher ces informations. -->
<?php 
    class Personne {
        public $nom;
        public $age;
      
        public function afficherNom() {
          echo "Le nom de la personne est : " . $this->nom;
        }
      
        public function afficherAge() {
          echo "L'âge de la personne est : " . $this->age;
        }
      }
    
      $personne = new Personne();
      $personne->nom = "Jean";
      $personne->age = 30;
      $personne->afficherNom(); // affiche "Le nom de la personne est : Jean"
      $personne->afficherAge(); // affiche "L'âge de la personne est : 30"
      
?>
</body>
</html>