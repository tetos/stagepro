<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>fonAvan1</title>
</head>
<body>
<!-- Créez une fonction calculerMoyenne qui prend un tableau de nombres en entrée et renvoie la moyenne. -->
<?php 
    function calculerMoyenne($tableau) {
        $somme = array_sum($tableau);
        $nombreElements = count($tableau);
        $moyenne = $somme / $nombreElements;
        return $moyenne;
      }

    $tableau = [10, 20, 30, 40, 50];
    $moyenne = calculerMoyenne($tableau);
    echo "La moyenne est : " . $moyenne; // affiche "La moyenne est : 30"

?>
</body>
</html>