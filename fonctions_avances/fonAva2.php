<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>fonAvan2</title>
</head>
<body>
    <!-- Écrivez une fonction estPalindrome qui vérifie si une chaîne de caractères est un palindrome (se lit de la même manière de gauche à droite et de droite à gauche). -->
    <?php 
        function estPalindrome($chaine) {
            $chaineInverse = strrev($chaine);
            if ($chaine == $chaineInverse) {
              return true;
            } 
              return false;
            
          }
          
          $chaine1 = "radar";
          if (estPalindrome($chaine1)) {
            echo $chaine1 . " est un palindrome.";
          } 
            echo $chaine1 . " n'est pas un palindrome.";
           // affiche "radar est un palindrome."
          
          $chaine2 = "bonjour";
          if (estPalindrome($chaine2)) {
            echo $chaine2 . " est un palindrome.";
          } 
            echo $chaine2 . " n'est pas un palindrome.";
           // affiche "bonjour n'est pas un palindrome."
          
    ?>
</body>
</html>