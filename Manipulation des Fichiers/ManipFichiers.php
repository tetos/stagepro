<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <?php 
        function lireFichier($nomFichier) {
            // Ouvre le fichier en mode lecture
            $fichier = fopen($nomFichier, "r");
          
            // Lit tout le contenu du fichier
            $contenu = fread($fichier, filesize($nomFichier));
          
            // Ferme le fichier
            fclose($fichier);
          
            // Renvoie le contenu du fichier
            return $contenu;
          }
          
    ?>
</body>
</html>