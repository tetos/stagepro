<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Variable_tableau</title>
</head>

<body>
    <!-- Créez une fonction nommée afficherNoms qui prend un tableau de noms d'amis en entrée et affiche chaque nom en utilisant une boucle -->
    <?php 
        function afficherNoms($noms) {
            foreach ($noms as $nom) {
                echo $nom . "<br>";
            }
        }
        $noms = array("Jean", "Marie", "Pauline", "Lucie");
        afficherNoms($noms);
    ?>
</body>

</html>