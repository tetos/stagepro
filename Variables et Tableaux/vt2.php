<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Variable_tableau</title>
</head>
<body>
    <!-- Créez une fonction nommée afficherAge qui prend l'âge en entrée et l'affiche. -->

    <?php 
        function afficherAge($age) {
            echo "L'âge est : " . $age . "<br>";
          }
          
          // Exemple d'utilisation :
          $age = 25;
          afficherAge($age);
          
    ?>
    
</body>
</html>