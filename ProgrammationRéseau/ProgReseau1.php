<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <!-- Créez un serveur web PHP simple qui écoute sur un port donné et renvoie une page HTML basique en réponse aux requêtes des navigateurs. -->
    <?php
    // Créer une socket d'écoute sur le port 8080
    $socket = socket_create_listen(8080);

    // Boucle d'écoute des requêtes
    while (true) {
        // Accepter la connexion entrante
        $client = socket_accept($socket);

        // Lire la requête HTTP envoyée par le navigateur
        $requete = socket_read($client, 1024);

        // Envoyer une réponse HTTP basique
        $reponse = "HTTP/1.1 200 OK\r\n";
        $reponse .= "Content-Type: text/html\r\n\r\n";
        $reponse .= "<html><head><title>Page de test</title></head><body><h1>Bienvenue sur ma page de test !</h1></body></html>";

        // Envoyer la réponse au navigateur
        socket_write($client, $reponse, strlen($reponse));

        // Fermer la connexion avec le navigateur
        socket_close($client);
    }
    ?>
    <!-- Explications :

- La première ligne du code crée une socket d'écoute sur le port 8080 en utilisant la fonction socket_create_listen.
- La boucle while écoute en permanence les requêtes entrantes en utilisant la fonction socket_accept. À chaque fois qu'une connexion est établie, la boucle lit la requête HTTP envoyée par le navigateur en utilisant la fonction socket_read.
- La réponse HTTP est stockée dans une variable $reponse. Dans cet exemple, il s'agit d'une page HTML basique qui affiche un titre et un message de bienvenue.
- La réponse est envoyée au navigateur en utilisant la fonction socket_write.
- Enfin, la connexion avec le navigateur est fermée en utilisant la fonction socket_close. -->
</body>

</html>