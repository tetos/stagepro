<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
<!-- Utilisez la bibliothèque cURL pour créer un script PHP qui effectue une demande HTTP GET vers un site web, récupère le contenu de la page et l'affiche. -->
    <?php
    // URL du site web à récupérer
    $url = "https://www.example.com";

    // Initialiser la session cURL
    $ch = curl_init();

    // Configurer les options de la session cURL
    curl_setopt($ch, CURLOPT_URL, $url); // URL de la requête
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); // Retourner la réponse dans une variable

    // Exécuter la requête HTTP GET et récupérer la réponse
    $response = curl_exec($ch);

    // Vérifier s'il y a des erreurs lors de l'exécution de la requête
    if (curl_errno($ch)) {
        echo "Erreur lors de la récupération de la page : " . curl_error($ch);
    } else {
        // Afficher le contenu de la page
        echo $response;
    }

    // Fermer la session cURL
    curl_close($ch);
    ?>
<!-- Explications :

- La première ligne du code définit l'URL du site web à récupérer.
- La fonction curl_init initialise une nouvelle session cURL.
- Les options de la session cURL sont configurées à l'aide de la fonction curl_setopt. Dans cet exemple, nous définissons l'URL de la requête et demandons à cURL de retourner la réponse dans une variable plutôt que de l'afficher directement.
- La fonction curl_exec exécute la requête HTTP GET et stocke la réponse dans une variable $response.
- Nous vérifions s'il y a des erreurs lors de l'exécution de la requête à l'aide de la fonction curl_errno. Si une erreur est détectée, nous affichons un message d'erreur.
- Si aucune erreur n'est détectée, nous affichons le contenu de la page en utilisant la variable $response.
- Enfin, la session cURL est fermée en utilisant la fonction curl_close. -->
</body>

</html>