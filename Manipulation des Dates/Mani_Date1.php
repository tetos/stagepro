<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Manipulation des Dates</title>
</head>
<body>
    <!-- Créez une fonction nommée afficherDateActuelle qui affiche la date actuelle au format "jour-mois-année" -->
    <?php 
        function afficherDateActuelle() {
            $date = date("d-m-Y");
            echo "La date actuelle est : " . $date . "<br>";
          }
          
          // Exemple d'utilisation :
          afficherDateActuelle();
          
    ?>
</body>
</html>