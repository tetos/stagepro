<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <!-- Créez une fonction nommée calculerAge qui prend la date de naissance en entrée et renvoie l'âge. -->
    <?php
        function calculAge($dateNaissance)
        {
            $arr1 = explode('/', $dateNaissance);
            $arr2 = explode('/', date('d/m/Y'));
                
            if(($arr1[1] < $arr2[1]) || (($arr1[1] == $arr2[1]) && ($arr1[0] <= $arr2[0])))
            return $arr2[2] - $arr1[2];
        
            return $arr2[2] - $arr1[2] - 1;
        }
        
        // Petit exemple
        $ma_date_de_naissance = '26/05/1987';
        $mon_age = calculAge($ma_date_de_naissance);
        

        
    
    ?>
    <p>vous avez  <?php echo $mon_age ?> ans</p>
</body>

</html>